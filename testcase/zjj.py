import random
import time
import sys
from config.basketball_name import basketball_list
from utils.handle_yaml import get_yamlCase_data
from utils.handle_path import data_path
from utils.handle_yaml import get_yaml_teamcheck
import os
from common.base import nba
from common.base import valueSum
from common.base import hit_rate



A_team_score = {}
B_team_score = {}
A_free_show_hit = []
B_free_show_hit = []
A_free_show_miss = []
B_free_show_miss = []
A_two_pointers_hit = []
B_two_pointers_hit = []
A_two_pointers_miss = []
B_two_pointers_miss = []
A_three_pointers_hit = []
B_three_pointers_hit = []
A_three_pointers_miss = []
B_three_pointers_miss = []

a = nba()
a.team_name()

nba_path = os.path.join(data_path,'nba.yaml')
get_yaml_teamcheck(fileDir= nba_path, user_inputA = a.ateam, user_inputB = a.bteam)
Ateam_name = get_yamlCase_data(fileDir=nba_path, team_choose= a.ateam)
Bteam_name = get_yamlCase_data(fileDir=nba_path, team_choose= a.bteam)

team = random.choice([a.ateam, a.bteam])

for round in range(0, 200):
    if team == a.ateam:
        name_choice = random.choices(Ateam_name, weights=[2, 2, 3, 1, 2])
        shoot_result = random.choices(["no_score", "one_score", "two_score", "three_score"], weights=[5.5, 1, 2, 1.5])
        if shoot_result == ["no_score"]:
            team = a.no_score(ball_choose=a.ateam, choose_name=Ateam_name, name_choice=name_choice, round=round,
                              choose_team_score=A_team_score, choose_two_miss= A_two_pointers_miss,
                              choose_three_miss=A_three_pointers_miss)
        elif shoot_result == ["one_score"]:
            team = a.one_shoot(ball_choose=a.ateam, name_choice=name_choice, round=round, choose_team_score= A_team_score,
                               choose_two_hit= A_two_pointers_hit, choose_free_hit= A_free_show_hit, choose_free_miss= A_free_show_miss)
        elif shoot_result == ["two_score"] :
            team = a.two_shoot(ball_choose=a.ateam, round=round, choose_team_score= A_team_score,
                               name_choice=name_choice, choose_two_hit= A_two_pointers_hit)
        elif shoot_result == ["three_score"]:
            team = a.three_shoot(ball_choose=a.ateam, round=round, name_choice=name_choice,
                                 choose_team_score=A_team_score, choose_three_hit=A_three_pointers_hit)
        print("\r比分A队 {}:{} B队  ".format(valueSum(A_team_score),valueSum(B_team_score)),end='')
        print(f'\nA队:{A_team_score}', end="")
        # time.sleep(3)
    elif team == a.bteam:
        name_choice = random.choices(Bteam_name, weights=[4, 2, 2, 1, 1])
        shoot_result = random.choices(["no_score", "one_score", "two_score", "three_score"], weights=[5.5, 1, 2, 1.5])
        if shoot_result  == ["no_score"]:
            team = a.no_score(ball_choose=a.bteam, choose_name=Bteam_name, name_choice=name_choice, round=round,
                              choose_team_score=B_team_score, choose_two_miss= B_two_pointers_miss,
                              choose_three_miss=B_three_pointers_miss)
        elif shoot_result == ["one_score"]:
            team = a.one_shoot(ball_choose=a.bteam, name_choice=name_choice, round=round,
                               choose_team_score=B_team_score,
                               choose_two_hit=B_two_pointers_hit, choose_free_hit=B_free_show_hit,
                               choose_free_miss=B_free_show_miss)
        elif shoot_result == ["two_score"]:
            team = a.two_shoot(ball_choose=a.bteam, round=round, choose_team_score=B_team_score,
                               name_choice=name_choice, choose_two_hit=B_two_pointers_hit)
        elif shoot_result == ["three_score"]:
            team = a.three_shoot(ball_choose=a.bteam, round=round, name_choice=name_choice,
                                 choose_team_score= B_team_score, choose_three_hit= B_three_pointers_hit)
        print("\r比分A队 {}:{} B队  ".format(valueSum(A_team_score), valueSum(B_team_score)), end='')
        print(f'\nB队:{B_team_score}', end="")
        # time.sleep(3)

print("\n{}队投篮命中率为{}/{},{}".format(a.ateam, len(A_two_pointers_hit+A_three_pointers_hit),len(A_two_pointers_hit+A_two_pointers_miss+A_three_pointers_hit+A_three_pointers_miss),hit_rate(A_two_pointers_hit+A_three_pointers_hit, A_two_pointers_miss+A_three_pointers_miss)))
print("{}队三分球命中率为{}/{},{}".format(a.ateam, len(A_three_pointers_hit),len(A_three_pointers_hit)+len(A_three_pointers_miss),hit_rate(A_three_pointers_hit,A_three_pointers_miss)))
print("{}队罚球命中率为{}/{},{}".format(a.ateam, len(A_free_show_hit),len(A_free_show_hit)+len(A_free_show_miss),hit_rate(A_free_show_hit,A_free_show_miss)))

print("\n{}队投篮命中率为{}/{},{}".format(a.bteam, len(B_two_pointers_hit+B_three_pointers_hit),len(B_two_pointers_hit+B_two_pointers_miss+B_three_pointers_hit+B_three_pointers_miss),hit_rate(B_two_pointers_hit+B_three_pointers_hit, B_two_pointers_miss+B_three_pointers_miss)))
print("{}队三分球命中率为{}/{},{}".format(a.bteam, len(B_three_pointers_hit),len(B_three_pointers_hit)+len(B_three_pointers_miss),hit_rate(B_three_pointers_hit,B_three_pointers_miss)))
print("{}队罚球命中率为{}/{},{}".format(a.bteam, len(B_free_show_hit),len(B_free_show_hit)+len(B_free_show_miss),hit_rate(B_free_show_hit,B_free_show_miss)))





#记录的其他笔记

# def fibonacci(n):  # 生成器函数 - 斐波那契
#     a, b, counter = 0, 1, 0
#     while True:
#         if (counter > n):
#             return
#         yield a
#         a, b = b, a + b
#         counter += 1

# f = fibonacci(10)  # f 是一个迭代器，由生成器返回生成
# #
# while True:
#     try:
#         print(next(f), end=" ")
#     except StopIteration:
#         sys.exit()

# def fab(max):
#     n, a, b = 0,0,1
#     while n < max:
#         yield b
#         a,b = b, a+b
#         n = n + 1
#
# c = fab(10)
# for i in fab(64):
#     print(i)



# test = 'acvbcnxd'
#
# test1 = test[:-1]
# print(test1)

# def hello():
#     return "hello world!"

# def makeitalic(fun):
#     def wrapped():
#         return "<i>" + fun() + "</i>"
#     return wrapped
#
# @makeitalic#使用了装饰器可以直接调用，不需要赋值了
# def hello():
#     return "hello world"
# print(hello())


# text = 'advcdsas'
#
# print(text[0])

# list1 = ['a','b','c','d','e']
# list1.insert(2,'v')
# print(list1)
# list1[2]='x'
# print(list1)
# list1.pop(2)
# print(list1)
# print(list1)
# list1_new = list1[-1:-4:-1]
# print(list1_new)
