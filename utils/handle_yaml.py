import yaml


def get_yaml_data(fileDir):
    with open(fileDir,encoding='utf-8') as fo:
        return yaml.safe_load(fo.read())
        #使用yaml读取yaml

def get_yamls_data(fileDir):#获取多段的yaml
    with open(fileDir,encoding='utf-8') as fo:
        return yaml.safe_load_all(fo.read())
        #使用yaml读取yaml

#-------写yaml-------------
def set_yaml_data(fileDir,inData):#获取yaml
    """
    :param fileDir: yaml文件路径
    :param inData: 写的数据
    """
    with open(fileDir,'a',encoding='utf-8') as fo:
        yaml.safe_dump(inData,fo)#使用yaml写入
#-------写分段yaml-------------
def set_yamls_data(fileDir,inData):#获取多段的yaml
    """
    :param fileDir: yaml文件路径
    :param inData: 写的数据
    """
    with open(fileDir,'w',encoding='utf-8') as fo:
        yaml.safe_dump_all(inData,fo)#使用yaml写入
#
def get_yamlCase_data(fileDir,team_choose):
    nameList = []
    with open(fileDir, encoding='utf-8') as fo:
        res = yaml.safe_load(fo.read())
        team = res.get(team_choose)
        for weizhi in team:
            nameList.append(team.get(weizhi))
    return nameList

def get_yaml_teamcheck(fileDir,user_inputA = None, user_inputB = None):
    with open(fileDir, encoding='utf-8') as fo:
        res = yaml.safe_load(fo.read())
        if user_inputA in res and user_inputB in res:
            pass
        else:
            print('你输入的球队不存在/有误')
            quit()

if __name__ == '__main__':
    get_yamlCase_data(fileDir='../data/nba.yaml',team_choose="湖人")
