import random
from utils.handle_yaml import get_yamlCase_data
from utils.handle_path import data_path
from utils.handle_yaml import get_yaml_teamcheck
import os

def valueSum(dict_sum):
    sum = 0
    for i in dict_sum:
        key = dict_sum[i]
        if type(key) is str:
            pass
        else:
            sum = sum + dict_sum[i]
    return sum

def hit_rate(hit,miss):
    if type(hit) == list and type(miss) == list:
        hit_len = len(hit)
        miss_len = len(miss)
        total = hit_len + miss_len
        if type(hit_len) and type(total) is int:
            percent = hit_len / total
            return "%.2f%%"%(percent*100)
        else:
            print("\n你的值不为数字")
    elif type(hit) == int and type(miss) == int:
        total = hit + miss
        percent = hit / total
        return "%.2f%%" % (percent * 100)
    elif type(hit) or type(miss) is not int:
        print("\n两个值都请输入list格式或者int格式")


class nba:
    def __init__(self,ateam=None,bteam=None):
        self.ateam = ateam
        self.bteam = bteam

    def team_name(self):
        self.ateam = input("请输入第一支球队名:")
        self.bteam = input("请输入第二支球队名:")

    def no_score(self, ball_choose, choose_name, name_choice, round, choose_team_score, choose_two_miss, choose_three_miss):
        score = 0
        backboard = random.choice([0, 0, 0, 1])
        if ball_choose == self.ateam:
            if backboard == 1:
                name_choice2 = random.choices(choose_name, weights=[2, 2, 3, 1, 2])
                choose_team_score[f"{name_choice}第{round + 1}回合投篮打铁,{name_choice2}抢到了前场篮板"] = score
                shoot_choose = random.choice(["two_shoot", "two_shoot", "three_shoot", "three_shoot"])
                if shoot_choose == "two_shoot":
                    choose_two_miss.append(1)
                elif shoot_choose == "three_shoot":
                    choose_three_miss.append(1)
                team = self.ateam
            else:
                choose_team_score[f"{name_choice}第{round + 1}回合投篮打铁,对方抢到了篮板球"] = score
                shoot_choose = random.choice(["two_shoot", "two_shoot", "three_shoot", "three_shoot"])
                if shoot_choose == "two_shoot":
                    choose_two_miss.append(1)
                elif shoot_choose == "three_shoot":
                    choose_three_miss.append(1)
                team = self.bteam
            return  team
        elif ball_choose == self.bteam:
            if backboard == 1:
                name_choice2 = random.choices(choose_name, weights=[4, 2, 2, 1, 1])
                choose_team_score[f"{name_choice}第{round + 1}回合投篮打铁,{name_choice2}抢到了前场篮板"] = score
                shoot_choose = random.choice(["two_shoot", "two_shoot", "three_shoot", "three_shoot"])
                if shoot_choose == "two_shoot":
                    choose_two_miss.append(1)
                elif shoot_choose == "three_shoot":
                    choose_three_miss.append(1)
                team = self.bteam
            else:
                choose_team_score[f"{name_choice}第{round + 1}回合投篮打铁,对方抢到了篮板球"] = score
                shoot_choose = random.choice(["two_shoot", "two_shoot", "three_shoot", "three_shoot"])
                if shoot_choose == "two_shoot":
                    choose_two_miss.append(1)
                elif shoot_choose == "three_shoot":
                    choose_three_miss.append(1)
                team = self.ateam
            return team
    def one_shoot(self,ball_choose, name_choice, round, choose_team_score, choose_two_hit, choose_free_hit, choose_free_miss):
        score = 1
        shoots_number = random.randrange(1, 4)
        if ball_choose == self.ateam:
            if shoots_number == 1:
                shoots_hit = random.choice(['命中', '命中', '命中', '打铁'])
                if shoots_hit == '命中':
                    choose_team_score[f"{name_choice}第{round + 1}回合投篮命中且加罚命中"] = score * 3
                    choose_two_hit.append(1)
                    choose_free_hit.append(1)
                elif shoots_hit == '打铁':
                    choose_team_score[f"{name_choice}第{round + 1}回合投篮命中但是加罚未命中"] = score * 2
                    choose_two_hit.append(1)
                    choose_free_miss.append(1)
                team = self.bteam
            elif shoots_number == 2:
                for i in range(0, 2):
                    shoots_hit = random.choice(['命中', '命中', '命中', '打铁'])
                    if shoots_hit == '命中':
                        choose_team_score["{}第{}回合获得{}次罚球机会，第{}罚命中".format(name_choice, round + 1, shoots_number, i + 1)] = score
                        choose_free_hit.append(1)
                    elif shoots_hit == '打铁':
                        choose_team_score["{}第{}回合获得{}次罚球机会,第{}罚打铁".format(name_choice, round + 1, shoots_number, i + 1)] = 0
                        choose_free_miss.append(1)
                team = self.bteam
            elif shoots_number == 3:
                for i in range(0, 3):
                    shoots_hit = random.choice(['命中', '命中', '命中', '打铁'])
                    if shoots_hit == '命中':
                        choose_team_score["{}第{}回合获得{}次罚球机会，第{}罚命中".format(name_choice, round + 1, shoots_number, i + 1)] = score
                        choose_free_hit.append(1)
                    if shoots_hit == '打铁':
                        choose_team_score["{}第{}回合获得{}次罚球机会,第{}罚打铁".format(name_choice, round + 1, shoots_number, i + 1)] = 0
                        choose_free_miss.append(1)
                team = self.bteam

        elif ball_choose == self.bteam:
            if shoots_number == 1:
                shoots_hit = random.choice(['命中', '命中', '命中', '打铁'])
                if shoots_hit == '命中':
                    choose_team_score[f"{name_choice}第{round + 1}回合投篮命中且加罚命中"] = score * 3
                    choose_two_hit.append(1)
                    choose_free_hit.append(1)
                elif shoots_hit == '打铁':
                    choose_team_score[f"{name_choice}第{round + 1}回合投篮命中但是加罚未命中"] = score * 2
                    choose_two_hit.append(1)
                    choose_free_miss.append(1)
                team = self.ateam
            elif shoots_number == 2:
                for i in range(0, 2):
                    shoots_hit = random.choice(['命中', '命中', '命中', '打铁'])
                    if shoots_hit == '命中':
                        choose_team_score[
                            "{}第{}回合获得{}次罚球机会，第{}罚命中".format(name_choice, round + 1, shoots_number, i + 1)] = score
                        choose_free_hit.append(1)
                    elif shoots_hit == '打铁':
                        choose_team_score[
                            "{}第{}回合获得{}次罚球机会,第{}罚打铁".format(name_choice, round + 1, shoots_number, i + 1)] = 0
                        choose_free_miss.append(1)
                team = self.ateam
            elif shoots_number == 3:
                for i in range(0, 3):
                    shoots_hit = random.choice(['命中', '命中', '命中', '打铁'])
                    if shoots_hit == '命中':
                        choose_team_score[
                            "{}第{}回合获得{}次罚球机会，第{}罚命中".format(name_choice, round + 1, shoots_number, i + 1)] = score
                        choose_free_hit.append(1)
                    if shoots_hit == '打铁':
                        choose_team_score[
                            "{}第{}回合获得{}次罚球机会,第{}罚打铁".format(name_choice, round + 1, shoots_number, i + 1)] = 0
                        choose_free_miss.append(1)
                team = self.ateam
        else:
            pass
        return team

    def two_shoot(self,ball_choose, round, choose_team_score, name_choice, choose_two_hit):
        score = 2
        if ball_choose == self.ateam:
            choose_team_score[f"{name_choice}第{round + 1}回合得分"] = score
            choose_two_hit.append(1)
            team = self.ateam
        elif ball_choose == self.bteam:
            choose_team_score[f"{name_choice}第{round + 1}回合得分"] = score
            choose_two_hit.append(1)
            team = self.bteam
        else:
            pass
        return team


    def three_shoot(self,ball_choose, round, name_choice, choose_team_score, choose_three_hit):
        if ball_choose == self.ateam:
            score = 3
            choose_team_score[f"{name_choice}第{round + 1}回合三分球命中！"] = score
            choose_three_hit.append(1)
            team = self.ateam
            return team
        elif ball_choose == self.bteam:
            score = 3
            choose_team_score[f"{name_choice}第{round + 1}回合三分球命中！"] = score
            choose_three_hit.append(1)
            team = self.bteam
            return team




# if __name__ == '__main__':
